var esModule = angular.module('esModule', []);

esModule.controller("visitantesController", function ($scope){
  $scope.listaVisitantes = [
    {nome: 'Daniel Marques', cpf:'25415233', apartamento:'402B'},
    {nome: 'Giovana Marques', cpf:'61651151', apartamento:'403B'},
    {nome: 'Patrícia Marques', cpf:'7411411', apartamento:'304A'},
    {nome: 'Domiciano Pereira', cpf:'74119684', apartamento:'104A'}
  ];

});
